# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Api do
  def app
    Api
  end

  describe '/status' do
    describe 'GET' do
      it 'returns ok' do
        get '/status'
        expect(last_response.body).to eq('OK')
        expect(last_response.status).to eq(200)
      end
    end
  end
end
