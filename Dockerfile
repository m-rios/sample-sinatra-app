FROM ruby:3.2.1-alpine AS development
ENV GEM_HOME="/usr/local/bundle"
ENV PATH $GEM_HOME/bin:$GEM_HOME/gems/bin:$PATH

# postgresql-client is only needed because rspec_job uses pg_isready to test if the database is up and running.
RUN apk update --no-cache && apk upgrade --no-cache && apk add --no-cache   ruby-bundler   ruby-bigdecimal   ruby-json   libpq   xz-dev gcompat  postgresql-client make gcc musl-dev

# Set the current working directory to /app
WORKDIR /app

# Copy both the gemfile and gemfile.lock here
COPY Gemfile* ./

# Build dependencies to compile nokogiri
RUN set -x 	&& apk upgrade --no-cache   && apk add --no-cache --virtual build-dependencies build-base libxml2-dev libxslt-dev postgresql-dev linux-headers && bundle install   && rm -rf /usr/local/bundle/bundler/gems/*/.git /usr/local/bundle/cache/ && apk del build-dependencies


# Copy everything else to the folder
COPY . .

ENTRYPOINT ["bundle"]
CMD ["exec", "rackup","config.ru","-o","0.0.0.0","-p","3000"]
