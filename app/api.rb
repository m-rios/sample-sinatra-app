# frozen_string_literal: true

require 'sinatra'

# Api class implements a simple api
class Api < Sinatra::Base
  get '/status' do
    'OK'
  end
end
